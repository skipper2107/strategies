<?php
namespace Skipper\Strategies\Strategies;

use Skipper\Strategies\Change;
use Skipper\Strategies\Contracts\StrategyAwareEntity;

abstract class UpdateStrategy extends GeneralStrategy
{
    /**
     * @param Change[] $changes
     * @param StrategyAwareEntity $entity
     * @return bool
     */
    public function isFit(array $changes, StrategyAwareEntity $entity): bool
    {
        return parent::isFit($changes, $entity)
            && null !== $entity->getId();
    }
}