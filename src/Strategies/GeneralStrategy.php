<?php
namespace Skipper\Strategies\Strategies;

use Skipper\Repository\Contracts\Repository;
use Skipper\Repository\Exceptions\StorageException;
use Skipper\Strategies\Change;
use Skipper\Strategies\Contracts\StrategyAwareEntity;
use Skipper\Strategies\Contracts\StrategyInterface;

abstract class GeneralStrategy implements StrategyInterface
{
    /**
     * @var Repository
     */
    protected $storage;

    public function __construct(Repository $repository)
    {
        $this->storage = $repository;
    }

    /**
     * @param Change[] $changes
     * @param StrategyAwareEntity $entity
     * @return bool
     */
    public function isFit(array $changes, StrategyAwareEntity $entity): bool
    {
        $className = $this->getEntityClassName();

        return false === empty($changes)
            && $entity instanceof $className
            && false === array_key_exists('id', $changes);
    }

    /**
     * @return string
     */
    abstract protected function getEntityClassName(): string;

    /**
     * @param StrategyAwareEntity $entity
     * @return void
     * @throws StorageException
     */
    public function apply(StrategyAwareEntity $entity): void
    {
        $this->storage->save($entity);
    }

}