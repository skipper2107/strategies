<?php
namespace Skipper\Strategies\Strategies;

use Psr\Log\LoggerInterface;
use Skipper\Strategies\Change;
use Skipper\Strategies\Contracts\StrategyAwareEntity;
use Skipper\Strategies\Contracts\StrategyInterface;

class DenyAll implements StrategyInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param StrategyAwareEntity $entity
     * @return void
     */
    public function apply(StrategyAwareEntity $entity): void
    {
        $this->logger->warning('DenyAll Strategy has been applied', [
            'entity' => get_class($entity),
            'entity_id' => $entity->getId(),
        ]);
    }

    /**
     * @param Change[] $changes
     * @param StrategyAwareEntity $entity
     * @return bool
     */
    public function isFit(array $changes, StrategyAwareEntity $entity): bool
    {
        return true;
    }
}