<?php
namespace Skipper\Strategies;

use Skipper\Strategies\Contracts\StrategyAwareEntity;

final class ChangesContext
{

    /**
     * @var StrategyAwareEntity
     */
    private $newEntity;

    /**
     * @var StrategyAwareEntity
     */
    private $oldEntity;

    public function __construct(StrategyAwareEntity $newEntity, StrategyAwareEntity $oldEntity)
    {
        $this->newEntity = $newEntity;
        $this->oldEntity = $oldEntity;
    }

    /**
     * @return StrategyAwareEntity
     */
    public function getNewEntity(): StrategyAwareEntity
    {
        return $this->newEntity;
    }

    /**
     * @return StrategyAwareEntity
     */
    public function getOldEntity(): StrategyAwareEntity
    {
        return $this->oldEntity;
    }
}