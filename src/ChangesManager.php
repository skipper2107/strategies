<?php
namespace Skipper\Strategies;

use Psr\Log\LoggerInterface;
use Skipper\Strategies\Contracts\StrategyAwareEntity;
use Skipper\Strategies\Contracts\StrategyInterface;
use Skipper\Strategies\Contracts\StrategyResolver;
use Skipper\Strategies\Strategies\DenyAll;

final class ChangesManager
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var StrategyResolver
     */
    private $strategyResolver;

    /**
     * ChangesManager constructor.
     * @param StrategyResolver $strategyResolver
     * @param LoggerInterface $logger
     */
    public function __construct(StrategyResolver $strategyResolver, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->strategyResolver = $strategyResolver;
    }

    /**
     * @param StrategyAwareEntity $entity
     * @return ChangesContext
     */
    public function initialize(StrategyAwareEntity $entity): ChangesContext
    {
        $context = new ChangesContext($entity, clone $entity);

        $this->logger->debug('Initialized change observing', [
            'entity' => get_class($entity),
        ]);

        return $context;
    }

    /**
     * @param ChangesContext $context
     * @return void
     */
    public function applyChanges(ChangesContext $context)
    {
        $changes = $this->getChanges($context);

        if (empty($changes)) {
            $this->logger->notice('Changed are empty');

            return;
        }

        $this->logger->debug('Changes found', [
            'changed_fields' => array_keys($changes),
        ]);

        $strategy = $this->chooseStrategy($changes, $context->getNewEntity());

        $this->logger->debug('Strategy found', [
            'strategy' => get_class($strategy),
        ]);

        $strategy->apply($context->getNewEntity());
    }

    /**
     * @param ChangesContext $context
     * @return Change[]
     */
    public function getChanges(ChangesContext $context): array
    {
        $oldObjectVars = $context->getOldEntity()->getObjectVars();
        $newObjectVars = $context->getNewEntity()->getObjectVars();

        $changes = [];

        foreach ($newObjectVars as $field => $value) {
            $compared = $oldObjectVars[$field];
            if ($value instanceof \DateTimeInterface && $compared instanceof \DateTimeInterface) {
                $isEqual = $value->format('U') === $compared->format('U');
            } else {
                $isEqual = $value === $compared;
            }
            if ($isEqual) {
                continue;
            }
            $changes[$field] = new Change($compared, $value);
        }

        return $changes;
    }

    /**
     * @param Change[] $changes
     * @param StrategyAwareEntity $entity
     * @return StrategyInterface
     */
    public function chooseStrategy(array $changes, StrategyAwareEntity $entity): StrategyInterface
    {
        foreach ($entity->getAvailableStrategies() as $strategyName) {
            /** @var StrategyInterface $strategy */
            $strategy = $this->strategyResolver->resolve($strategyName);
            if ($strategy->isFit($changes, $entity)) {
                return $strategy;
            }
        }

        return $this->strategyResolver->resolve(DenyAll::class);
    }
}