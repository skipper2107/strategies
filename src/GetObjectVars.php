<?php
namespace Skipper\Strategies;

trait GetObjectVars
{
    /**
     * @return array
     */
    public function getObjectVars(): array
    {
        return get_object_vars($this);
    }
}