<?php
namespace Skipper\Strategies\Contracts;

use Skipper\Repository\Contracts\Entity;

interface StrategyAwareEntity extends Entity
{
    /**
     * @return string[]
     */
    public function getAvailableStrategies(): array;

    /**
     * @return array
     */
    public function getObjectVars(): array;
}