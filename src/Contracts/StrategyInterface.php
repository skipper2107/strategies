<?php
namespace Skipper\Strategies\Contracts;

use Skipper\Strategies\Change;

interface StrategyInterface
{
    /**
     * @param Change[] $changes
     * @param StrategyAwareEntity $entity
     * @return bool
     */
    public function isFit(array $changes, StrategyAwareEntity $entity): bool;

    /**
     * @param StrategyAwareEntity $entity
     * @return void
     */
    public function apply(StrategyAwareEntity $entity): void;
}