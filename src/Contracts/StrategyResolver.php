<?php
namespace Skipper\Strategies\Contracts;

interface StrategyResolver
{
    /**
     * @param string $className
     * @return StrategyInterface
     */
    public function resolve(string $className): StrategyInterface;
}