<?php
namespace Skipper\Strategies;

class Change
{
    /**
     * @var mixed
     */
    public $from;

    /**
     * @var mixed
     */
    public $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }
}